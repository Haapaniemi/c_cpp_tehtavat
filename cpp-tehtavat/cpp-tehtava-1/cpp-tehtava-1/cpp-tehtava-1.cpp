// cpp-tehtava-1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
    std::cout << "Hello World!\n"; 
}

/* Tehtävä 1. Luettele seuraavien olioiden tietorakenteita ja operaatioita:
	
	a) Hissi
		Rakenne (luokka) ja sen tarjolla olevat operaatiot voisi olla seuraavanlainen:

		class Hissi {
			private:
				int id;
				char tila;
				int kerros;
			
			public:
				Hissi(int id);
				void siirrykerrokseen(int kerros);
				void avaaHissinOvet();
		}


	b) (Tietojaan kysyvä) opiskelija
		Rakenne (luokka) ja sen tarjolla olevat operaatiot voisi olla seuraavanlainen:

		class Opiskelija {
			private:
				int opiskelijatunnus;
				int opintopisteet;

			public:
				void tulostaOpiskelijatunnus();
				void tulostaOpintopisteet();
		}

	c) Kahvinkeitin
		Rakenne (luokka) ja sen tarjolla olevat operaatiot voisi olla seuraavanlainen:
		
		class Kahvinkeitin {
			private:
				int id;
				int tila;
				muutaTilaa(int uusiTila);

			public:
				Kahvinkeitin(int id);
				keitäKahvia(muutaTilaa(1));
		}

	d) Syöttöikkuna
		Rakenne (luokka) ja sen tarjolla olevat operaatiot voisi olla seuraavanlainen:

		Class Ikkuna{};

		class Syöttöikkuna {
			private:
				Ikkuna tämänhetkinenIkkuna;
				Ikkuna edellinenIkkuna;
				void muutaIkkunaa();
				char syötteet[];

			public:
				void siirryEdelliseenIkkunaan(muutaIkkunaa());
				void talletaSyöte(char syöte);
				void päivitäNäkymä();
		}






// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
*/