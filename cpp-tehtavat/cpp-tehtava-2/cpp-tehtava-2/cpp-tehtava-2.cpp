// cpp-tehtava-2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "clocale"
using namespace std;


char nimi[51]; // globaali muuttuja (näkyy suoraan kaikille funktioille tästä alaspäin)
int ika;

void kysyNimi()
{
	cout << " Kysy nimi ";
	cin >> nimi;
}
void kysyIka()
{
	cout << " Kysy ikä ";
	cin >> ika;
}
void tulostaNimi()
{
	cout << " Tulostetaan nimi " << nimi << endl;
}
void tulostaIka()
{
	cout << " Tulostetaan ikä " << ika << endl;
}


void tulostaValikko()
{
	char v;
	do
	{
		//system("cls"); // tyhjennä näyttö // piilottaa vastaukset muutoin.
		cout << "---------------------------------Testataan IO:ta------------------------------";
		cout << "\n\n\n\n";
		cout << "                          kysy nimi\t\t1";
		cout << "\n                          kysy ikä\t\t2";
		cout << "\n                          tulosta nimi\t\t3";
		cout << "\n                          tulosta ikä\t\t4";
		cout << "\n                          Lopetus\t\t5";
		cout << "\n\n\n                            Valitse:";
		cin >> v;
		switch (v) {
		case '1': 	kysyNimi();
			break;
		case '2': 	kysyIka();
			break;
		case '3':   tulostaNimi();
			break;
		case '4': 	tulostaIka();
			break;
		case '5':
			break;

		}
	} while (v != '5');
}



int main()
{
	setlocale(LC_ALL, "FI-fi");
	tulostaValikko();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
