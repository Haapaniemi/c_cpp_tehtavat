// cpp-tehtava-3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "clocale"
#include <string>
#include <memory>
#include <time.h>
#include <vector>
using namespace std;

typedef enum matkantyyppi { EIMAARITETTY, SISAINEN, SEUTU };

class Leima {
	public:
		time_t aikaleima;
		matkantyyppi matkanTyyppi;
		string kortinOmistajanNimi;
		Leima() {};
		void lisääAikaleima() {
			aikaleima = time(NULL);
		};
		void lisääMatkantyyppi(matkantyyppi tyyppi) {
			matkanTyyppi = tyyppi;
		};
		void lisää_kortin_omistajan_nimi(string nimi) {
			kortinOmistajanNimi = nimi;
		};
};

class Leimaaja {
	private:
		int linjanNumero;
		vector<Leima> leimaukset;
		Leima edellinenLeima;
	public:
		Leimaaja() {
			cout << "Syötä leimaajan linjan numero: " << endl;
			cin >> linjanNumero;
		};

		// Rakensin leimauksen vähän eri tavalla, eli leimassa on matkantyyppi ja omistajan nimi
		// eikä leimaajassa.
		Leimaaja operator<<(const string nimi) {
			shared_ptr<Leima> leima = make_shared<Leima>();
			leima->lisääAikaleima();
			leima->lisää_kortin_omistajan_nimi(nimi);
			matkantyyppi tyyppi = SISAINEN;
			leima->lisääMatkantyyppi(tyyppi);
			leimaukset.push_back(*leima);
			edellinenLeima = *leima;
			return *this;
		};

		void leimaa(string nimi, matkantyyppi matkan_tyyppi) {
			shared_ptr<Leima> leima = make_shared<Leima>();
			leima->lisääAikaleima();
			leima->lisää_kortin_omistajan_nimi(nimi);
			leima->lisääMatkantyyppi(matkan_tyyppi);
			leimaukset.push_back(*leima);
			edellinenLeima = *leima;
		};

		Leima palautaEdellinenLeima() {
			return edellinenLeima;
		};

		void tulostaLeimaukset() {
			for (int i = 0; i < leimaukset.size(); i++) {
				cout << "Kortin omistaja: " << leimaukset[i].kortinOmistajanNimi << endl;
				#pragma warning(push)
				#pragma warning(disable: 4996)
				string aikaleima = asctime(localtime(&leimaukset[i].aikaleima));
				cout << "Aikaleima: " << aikaleima << endl;
				#pragma warning(pop)
			}
		};
};


class Matkakortti {
	private:
		// Luodaan muuttujat osoittimina
		/*
		string* haltijanNimi;
		double* saldo;
		int* kortinId;
		*/
		// Ja älykkäinä osoittimina 'unique_ptr'
		unique_ptr<string> haltijanNimi;
		unique_ptr<double> saldo;
		unique_ptr<int> kortinId;
	public:
		Matkakortti() {
			// Varataan muuttujille tila kasasta alustamalla muuttujat new:lla.
			/*
			haltijanNimi = new string;
			saldo = new double;
			kortinId = new int;
			*/
			// Älykkäät osoittimet
			haltijanNimi = make_unique<string>();
			saldo = make_unique<double>();
			kortinId = make_unique<int>();
		};
		// Destruktori, joka vapauttaa varatut tilat kasasta kun matkakortti tuhoutuu.
		~Matkakortti() {
			cout << "Destruktori kutsuttu, vapautetaan osoittimien varaamat tilat kasasta" << endl;
			/*
			delete haltijanNimi;
			delete saldo;
			delete kortinId;*/
		}
		// Kaatuu jos muuttujien osoittimet poistettu desktruktorilla
		// eli se ainakin toimii.
		void tulostaTiedot() {
			cout << "Nimi = " << *haltijanNimi << endl;
			cout << "Saldo = " << *saldo << endl;
			cout << "KortinID = " << *kortinId << endl;
		}

		string getNimi() {
			return *haltijanNimi;
		};

		void matkakortinAlustus() {
			cout << "Syötä kortin haltijan nimi:" << endl;
			cin >> *haltijanNimi;
			cout << "Syötä kortin id:" << endl;
			cin >> *kortinId;
			cout << "Luotu Matkakortti, haltijan nimi = " << *haltijanNimi;
			cout << " Kortin ID = " << *kortinId << endl;
		};
		
		void matkakortinLataus() {
			cout << "Paljonko saldoa ladataan kortille?: " << endl;
			cin >> *saldo;
			cout << "Matkakortin uusi saldo = " << *saldo << endl;
		};

		void matkustaKortilla(shared_ptr<Leimaaja> leimaaja) {
			int valinta = 0;

			// Lippujen hinnat
			double sisainenlippu_hinta = 2.50;
			double seutulippu_hinta = 3.80;

			// Matkojen tyypit
			enum matkantyyppi sisainen = SISAINEN;
			enum matkantyyppi seutu = SEUTU;

			// Lipun valinta
			cout << "Lipun osto, 1 = sisäinen & 2 = seutu" << endl;
			cin >> valinta;
			if (valinta == 1) {
				// Saldon tarkistus
				if (*saldo > sisainenlippu_hinta) {
					// Matkakortin leimaus
					leimaaja->leimaa(*haltijanNimi, sisainen);
					cout << "Matkakortti leimattu onnistuneesti." << endl;
					// Leimauksen tarkistus
					Leima edellinenLeima = leimaaja->palautaEdellinenLeima();
					string leimatunNimi = edellinenLeima.kortinOmistajanNimi;
					// asctime deprecated, mscv ei halua compilea jos varoitusta ei sivuuteta.
					#pragma warning(push)
					#pragma warning(disable: 4996)
					string aikaleima = asctime(localtime(&edellinenLeima.aikaleima));
					#pragma warning(pop)
					cout << "Leimatun matkakortin omistajannimi = " << leimatunNimi << endl;
					cout << "Leiman aikaleima = " << aikaleima << endl;
					// Lipun osto
					*saldo -= sisainenlippu_hinta;
					cout << "Sisainen lippu ostettu, matkakortin uusi saldo = " << *saldo << endl;
				}
				else {
					cout << "Matkakortilla ei ollut tarpeeksi saldoa lipun ostoon.";
				}
			}
			else if (valinta == 2) {
				// Saldon tarkistus
				if (*saldo > seutulippu_hinta) {
					// Matkakortin leimaus
					leimaaja->leimaa(*haltijanNimi, sisainen);
					cout << "Matkakortti leimattu onnistuneesti." << endl;
					// Leimauksen tarkistus
					Leima edellinenLeima = leimaaja->palautaEdellinenLeima();
					string leimatunNimi = edellinenLeima.kortinOmistajanNimi;
					// asctime deprecated, mscv ei halua compilea jos varoitusta ei sivuuteta.
					#pragma warning(push)
					#pragma warning(disable: 4996)
					string aikaleima = asctime(localtime(&edellinenLeima.aikaleima));
					#pragma warning(pop)
					cout << "Leimatun matkakortin omistajannimi = " << leimatunNimi << endl;
					cout << "Leiman aikaleima = " << aikaleima << endl;
					// Lipun osto
					*saldo -= seutulippu_hinta;
					cout << "Seutu lippu ostettu, matkakortin uusi saldo = " << *saldo << endl;
				}
				else {
					cout << "Matkakortilla ei ollut tarpeeksi saldoa lipun ostoon";
				}
			}
		};

};

int main()
{
	setlocale(LC_ALL, "FI-fi");
	int input = 0;
	// luodaan matkakortti pinoon
	/*
	Matkakortti matkakortti;*/
	// Älykäs osoitin matkakorttiin
	shared_ptr<Matkakortti> matkakortti = make_shared<Matkakortti>();
	shared_ptr<Leimaaja> leimaaja = make_shared<Leimaaja>();

	while (input != 8) {
		cout << "\n1. Matkakortin alustus" << endl;
		cout << "2. Matkakortin lataus" << endl;
		cout << "3. Kortilla matkustaminen" << endl;
		cout << "4. Kutsu matkakortin destruktoria" << endl;
		cout << "5. Tulosta matkakortin tiedot" << endl;
		cout << "6. Tulosta leimaajan muistissa olevat leimaukset" << endl;
		cout << "7. Leimaaja << kortti" << endl;
		cout << "8. Lopeta" << endl;
		cin >> input;

		if (input == 1) {
			matkakortti->matkakortinAlustus();
		}
		else if (input == 2) {
			matkakortti->matkakortinLataus();
		}
		else if (input == 3) {
			matkakortti->matkustaKortilla(leimaaja);
		}
		else if(input == 4) {
			matkakortti->~Matkakortti();
		}
		else if (input == 5) {
			matkakortti->tulostaTiedot();
		}
		else if (input == 6) {
			leimaaja->tulostaLeimaukset();
		}
		else if (input == 7) {
			*leimaaja << matkakortti->getNimi();
		}
	}
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
