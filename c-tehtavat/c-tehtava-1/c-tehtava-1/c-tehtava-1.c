// c-tehtava-1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "clocale"


void tulostaValikko(){
	//variables
	int input = 0;
	char nimi[50];
	int ika = 0;
	
	//Loop for the operands
	while (input != 5) {
		printf("\n1. Kysy nimi\n");
		printf("2. Kysy ikä\n");
		printf("3. Tulosta nimi\n");
		printf("4. Tulosta ikä\n");
		printf("5. Exit\n");

		scanf_s(" %d", &input);

		if (input == 1) {
			printf("\nSyötä nimesi: ");
			scanf_s(" %s", nimi, _countof(nimi));
		}
		else if (input == 2) {
			printf("Syötä ikäsi: ");
			scanf_s(" %d", &ika);
		}
		else if (input == 3) {
			printf(" %s\n", &nimi);
		}
		else if (input == 4) {
			printf(" %d\n", ika);
		}

	}
	
}

int main() {
	setlocale(LC_ALL, "fi-FI");
	tulostaValikko();
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
