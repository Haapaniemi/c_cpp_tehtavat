// c-tehtava-4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <time.h>

int main()
{
	// Initializing the variables
	int numbers[6];
	int number1 = 0, number2 = 0, number3 = 0;
	srand(time(NULL));
	int random1 = rand();
	int random2 = rand();
	int random3 = rand();
	int *numberpointer;

	// Asking for integer inputs
	printf("Anna 1. kokonaisluku: ");
	scanf_s(" %d", &number1);
	printf("Anna 2. kokonaisluku: ");
	scanf_s(" %d", &number2);
	printf("Anna 3. kokonaisluku: ");
	scanf_s(" %d", &number3);

	// populating the array
	numbers[0] = number1;
	numbers[1] = number2;
	numbers[2] = number3;
	numbers[3] = random1;
	numbers[4] = random2;
	numbers[5] = random3;

	// printing the values and addresses on the array elements
	for (numberpointer = numbers; numberpointer < (&numbers)[1]; numberpointer ++) {
		printf("Element = %d. The address of element in hexadecimal = %p and in decimal = %d\n", *numberpointer, numberpointer, numberpointer);
	}


}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
