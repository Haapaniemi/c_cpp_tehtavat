// c-tehtava-2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>


void reverse_name_and_print(char *name) {
	char *pointerStart = name;
	char *pointerEnd = pointerStart + strlen(pointerStart) - 1;

	// Capitalizing the string
	char *a = name;
	while (*a) {
		*a = toupper(*a);
		a++;
	}
	
	while (pointerEnd > pointerStart) {
		char y = *pointerEnd;
		*pointerEnd-- = *pointerStart;
		*pointerStart++ = y;
	}
	printf("%s", name);
}

int main() {
	char name[50];
	printf("Input a string to be reversed: ");
	scanf_s(" %s", name, _countof(name));
	reverse_name_and_print(name);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
