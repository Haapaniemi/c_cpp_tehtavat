// c-tehtava-5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "clocale"

struct Matkakortti {
	char haltijanNimi[20];
	double saldo;
	int kortinId;
} Matkakortti_default = { "default", 0.0, 0 };

void matkakortinAlustus(struct Matkakortti matkakortti) {
	printf("Syötä kortin haltijan nimi: \n");
	scanf_s(" %s", &matkakortti.haltijanNimi);
	printf("Syötä kortin id: \n");
	scanf_s(" %d", &matkakortti.kortinId);
}

void matkakortinLataus(struct Matkakortti matkakortti) {
	printf("Paljonko saldoa ladataan kortille?: ");
	scanf_s(" %lf", matkakortti.saldo);
	printf("Matkakortin uusi saldo = %lf\n", matkakortti.saldo);
}

void matkustaKortilla(struct Matkakortti matkakortti) {
	int valinta = 0;
	double sisainen_lippu_hinta = 2.50;
	double seutu_lippu_hinta = 3.80;
	printf("Lipun osto, 1=sisäinen & 2=seutu : ");
	scanf_s(" %d", &valinta);

	if (valinta == 1) {
		printf("IF = 1, saldo = %lf", matkakortti.saldo);
		if (matkakortti.saldo > sisainen_lippu_hinta) {
			matkakortti.saldo = matkakortti.saldo - sisainen_lippu_hinta;
			printf("Sisäisen lipun osto onnistui, saldoa vähennetty 2.50.\n");
		}
		else {
			printf("Kortilla ei ollut tarpeeksi saldoa lipun ostoon.\n");
		}
	}
	else if (valinta == 2) {
		if (matkakortti.saldo > seutu_lippu_hinta) {
			matkakortti.saldo = matkakortti.saldo - seutu_lippu_hinta;
			printf("Seutu lipun osto onnistui, saldoa vähennetty 3.80.\n");
		}
		else {
			printf("Kortilla ei ollut tarpeeksi saldoa lipun ostoon.\n");
		}
	}
}


int main()
{
	setlocale(LC_ALL, "fi-FI");
	int input = 0;
	struct Matkakortti matkakortti = Matkakortti_default;

	while (input != 5) {
		printf("1. Matkakortin alustus\n");
		printf("2. Matkakortin lataus\n");
		printf("3. Kortilla matkustaminen\n");
		printf("5. Lopeta\n");
		scanf_s(" %d", &input);

		if (input == 1) {
			matkakortinAlustus(matkakortti);
		}
		else if (input == 2) {
			matkakortinLataus(matkakortti);
		}
		else if (input == 3) {
			matkustaKortilla(matkakortti);
		}
		else {
			printf("Epäkelpo syöte!\n");
		}
	}
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
