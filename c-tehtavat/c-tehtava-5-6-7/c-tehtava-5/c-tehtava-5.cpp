// c-tehtava-5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "clocale"
#include <time.h>




// Tietorakenne matkakortti
struct Matkakortti {
	char haltijanNimi[256];
	double saldo[256];
	int kortinId[256];
} Matkakortti_default = {"default", 0, 0};

// Matkojen enum tyypit
typedef enum matkantyyppi { EIMAARITETTY, SISAINEN, SEUTU };

struct Leima {
	time_t aikaleima;
	matkantyyppi matkanTyyppi;
} Leima_default = { 0, EIMAARITETTY };

struct Leimaaja {
	char leimaajanNimi[256];
	int linjanNumero[256];
	Leima leimaukset[256];
} Leimaaja_default = {"default", 0, 0};





void matkakortinAlustus(struct Matkakortti* korttipointer) {
	printf("Syötä kortin haltijan nimi: \n");
	// SCANF_S näköjään vaatii päätyosoitteen taulun koon. '_countof' tai int.
	scanf_s(" %s", korttipointer->haltijanNimi, _countof(korttipointer->haltijanNimi));
	printf("Syötä kortin id: \n");
	scanf_s(" %d", korttipointer->kortinId, korttipointer->kortinId);
}

void matkakortinLataus(struct Matkakortti* korttipointer) {
	printf("Paljonko saldoa ladataan kortille?: ");
	scanf_s(" %lf", korttipointer->saldo, sizeof(korttipointer->saldo));
	printf("Matkakortin uusi saldo = %lf\n",*korttipointer->saldo);
}

void matkustaKortilla(struct Matkakortti* korttipointer, struct Leimaaja* leimaajapointer) {
	int valinta = 0;
	// Lippujen hinnat
	double sisainen_lippu_hinta = 2.50;
	double seutu_lippu_hinta = 3.80;
	double* sisainenPointer = &sisainen_lippu_hinta;
	double* seutuPointer = &seutu_lippu_hinta;
	
	// Asetetaan linjan numero leimaajalle
	printf("Syötä linjan numero: ");
	scanf_s(" %d", leimaajapointer->linjanNumero, sizeof(leimaajapointer->linjanNumero));

	printf("Lipun osto, 1=sisäinen & 2=seutu : ");
	scanf_s(" %d", &valinta, valinta);

	if (valinta == 1) {
		if (*korttipointer->saldo > *sisainenPointer) {
			*korttipointer->saldo -= *sisainenPointer;

			// Matkakortin leimaus
			struct Leima leima = Leima_default;
			leima.aikaleima = time(NULL);
			leima.matkanTyyppi = SISAINEN;
			// Lisätään leima leimaajan tauluun.
			leimaajapointer->leimaukset[sizeof(*leimaajapointer->leimaukset) - 1] = leima;
			// Tarkistetaan, että matkantyyppi on todella SISAINEN
			enum matkantyyppi tyyppi = SISAINEN;
			char matkan_tyyppi[] = "EI TIEDOSSA";
			if (int(leimaajapointer->leimaukset[sizeof(*leimaajapointer->leimaukset) - 1].matkanTyyppi) == int(tyyppi)) {
				#pragma warning(push)
				#pragma warning(disable: 4996)
				strcpy(matkan_tyyppi, "SISAINEN");
				#pragma warning(pop)
			}
			// Piilotetaan msvc deprecated varoitukset asctime():sta.
			#pragma warning(push)
			#pragma warning(disable: 4996)
			printf("Matkakortti leimattu onnistuneesti. Leiman tyyppi = %s ja aikaleima = %s", matkan_tyyppi, asctime(localtime(&leimaajapointer->leimaukset[sizeof(*leimaajapointer->leimaukset) - 1].aikaleima)));
			#pragma warning(pop)
			
			printf("Sisäisen lipun osto onnistui, saldoa vähennetty 2.50.\n");
			printf("Matkakortin uusi saldo = %lf\n", *korttipointer->saldo);
		}
		else {
			printf("Kortilla ei ollut tarpeeksi saldoa lipun ostoon.\n");
		}
	}
	else if (valinta == 2) {
		if (*korttipointer->saldo > *seutuPointer) {
			*korttipointer->saldo -= *seutuPointer;

			// Matkakortin leimaus
			struct Leima leima = Leima_default;
			leima.aikaleima = time(NULL);
			leima.matkanTyyppi = SEUTU;
			// Lisätään leima leimaajan tauluun.
			leimaajapointer->leimaukset[sizeof(*leimaajapointer->leimaukset) - 1] = leima;
			// Tarkistetaan, että matkantyyppi on todella SEUTU
			enum matkantyyppi tyyppi = SEUTU;
			char matkan_tyyppi[] = "EI TIEDOSSA";
			if (int(leimaajapointer->leimaukset[sizeof(*leimaajapointer->leimaukset) - 1].matkanTyyppi) == int(tyyppi)) {
				#pragma warning(push)
				#pragma warning(disable: 4996)
				strcpy(matkan_tyyppi, "SEUTU");
				#pragma warning(pop)
			}
			// Piilotetaan msvc deprecated varoitukset asctime():sta.
			#pragma warning(push)
			#pragma warning(disable: 4996)
			printf("Matkakortti leimattu onnistuneesti. Leiman tyyppi = %s ja aikaleima = %s", matkan_tyyppi, asctime(localtime(&leimaajapointer->leimaukset[sizeof(*leimaajapointer->leimaukset) - 1].aikaleima)));
			#pragma warning(pop)

			printf("Seutu lipun osto onnistui, saldoa vähennetty 3.80.\n");
			printf("Matkakortin uusi saldo = %lf\n", *korttipointer->saldo);
		}
		else {
			printf("Kortilla ei ollut tarpeeksi saldoa lipun ostoon.\n");
		}
	}
}


int main()
{
	setlocale(LC_ALL, "fi-FI");
	int input = 0;
	struct Matkakortti matkakortti = Matkakortti_default;
	struct Matkakortti* korttipointer = &matkakortti;

	struct Leimaaja leimaaja = Leimaaja_default;
	struct Leimaaja* leimaajapointer = &leimaaja;


	while (input != 5) {
		printf("1. Matkakortin alustus\n");
		printf("2. Matkakortin lataus\n");
		printf("3. Kortilla matkustaminen\n");
		printf("5. Lopeta\n");
		scanf_s(" %d", &input);

		if (input == 1) {
			matkakortinAlustus(korttipointer);
		}
		else if (input == 2) {
			matkakortinLataus(korttipointer);
		}
		else if (input == 3) {
			matkustaKortilla(korttipointer, leimaajapointer);
		}
		else {
			printf("Epäkelpo syöte!\n");
		}
	}
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
