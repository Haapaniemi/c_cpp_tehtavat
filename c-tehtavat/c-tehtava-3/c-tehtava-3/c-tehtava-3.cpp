// c-tehtava-3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "clocale"

int main()
{
	setlocale(LC_ALL, "fi-FI");

	char merkkijono1[60];
	char merkkijono2[60];
	char i, j;
	
	printf("Syötä kaksi merkkijonoa, ensimmäinen: ");
	scanf_s("%s", merkkijono1, _countof(merkkijono1));
	printf("Ja toinen: ");
	scanf_s("%s", merkkijono2, _countof(merkkijono2));
	
	//ensimmäisen merkkijonon pituus
	i = strlen(merkkijono1);

	//yhdistetään toinen merkkijono ensimmäisen merkkijonon jatkeeksi for loopissa
	// Jos käytetty puskuri on liian pieni, epäonnistuu/kaatuu alla oleva looppi
	for (j = 0; merkkijono2[j] != '\0'; ++j, ++i) {
		merkkijono1[i] = merkkijono2[j];
	}

	//merkataan merkkijonon päättyminen
	merkkijono1[i] = '\0';
	printf("\n Yhdistetty: %s", merkkijono1);
	
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
